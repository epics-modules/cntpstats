# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <registryFunction.h>
# include <epicsExport.h>
# include <aSubRecord.h>
# include <time.h>

static int sources(aSubRecord *precord) {
    FILE *fp;
    char cmd_output[1000];
    char data[20][100];
    int  stringlen[20];
    char *token;
    int i = 0;
    int j = 0;
    char empty_line = '1';
    char server_status;
    char normal_output[20] = "sources";
    time_t now;
    char chrony_cmd[50] = "chronyc sources";
    char error_string[1000];
    int server_connection = 0;
    int error_flag = 0;

    // Get time
    time(&now);

    // Open the command for reading
    fp = popen(chrony_cmd, "r");
    if (fp == NULL) {
        printf("Failed to run command\n");
        pclose(fp);
        return 0;
    }

    memset(cmd_output, '\0', sizeof(cmd_output));
    if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL) {
        empty_line = '\0';
    }
    
    // If normal output not found, set error 
    if (strstr(cmd_output,normal_output) == NULL) {
        //printf("%s\n%s%s\n", chrony_cmd, ctime(&now), cmd_output);
        memset(error_string, '\0', sizeof(error_string));
        strcat(error_string, chrony_cmd);
        strcat(error_string, cmd_output);
        strcat(error_string, ctime(&now));
        error_flag = 1;
    }

    // Read the output a line at a time - output it.
    while (empty_line != '\0' && error_flag == 0) {
        //printf("%s\n", cmd_output);
        // Split the string using token delimiters
        token = strtok(cmd_output," []");
        if (strlen(cmd_output) > 0) {
            server_status = cmd_output[1];
        } else {
            server_status = '\0';
        }
        // Parse row and look for connected server with '*'
        while(token != NULL && server_status == '*') {
            server_connection = 1;
            memset(data[i], '\0', sizeof(data[i]));
            stringlen[i] = strlen(token);
            for (j = 0; j < stringlen[i]; j++) {
                if( token[j] == '\n' ) {
                    token[j] = 0;
                }
                data[i][j] = token[j];
            }

            i++;
            token = strtok(NULL, " []");
        }

        if (server_status == '*') {
            // Format data
            strcat(data[6],"[");
            strcat(data[6],data[7]);
            strcat(data[6],"]");
            strcat(data[6],data[8]);
            strcat(data[6],data[9]);
            //strcat(data[6],data[10]);
            //printf("%d,%s,%d\n", server_connection,data[1],atoi(data[2]));
        }
        // Get the next output line
        memset(cmd_output, '\0', sizeof(cmd_output));
        if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL) {
           empty_line = '\0';
        }
    }

    pclose(fp);
    
    if (error_flag == 0) {
        //server connected
        *(long *)precord->vala = server_connection;
        //name_ip
        strcpy(precord->valb, data[1]);
        //stratum level
        *(long *)precord->valc = atoi(data[2]);
        //poll rate
        *(long *)precord->vald = atoi(data[3]);
        //reach
        *(long *)precord->vale = atoi(data[4]);
        //last received
        *(long *)precord->valf = atoi(data[5]);
        //last sample
        strcpy(precord->valg, data[6]);
    }
    // Error flag
    *(long *)precord->valh = error_flag;
    if (error_flag == 1) {
        // Error string
        strcpy(precord->vali, error_string);
    }
    return 0;
}

epicsRegisterFunction(sources);
